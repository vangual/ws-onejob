-----------------------------------------------------------------------------------------------
-- The MIT License (MIT)

-- Copyright (c) 2014 Vangual

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-----------------------------------------------------------------------------------------------

local OneJob = Apollo.GetPackage("Gemini:Addon-1.0").tPackage:NewAddon("OneJob", "OneJob", {"Gemini:Logging-1.2", "Gemini:GUI-1.0"}, "Gemini:Bucket-1.0")

OneJob.addon_name = 'OneJob'
OneJob.addon_version = '0.6.1'

local log = {}

---------------
-- Fallback dummy logging. These should never be used. They're overwritten with GeminiLogging upon Init
function log:error(msg)
	print('ERROR '..msg)
end
function log:warn(msg)
	print('WARN '..msg)
end
function log:debug(msg)
	--print('DEBUG '..msg)
end
function log:info(msg)
	--print('INFO '..msg)
end

----------------------------------
-- Constants
local class_icons =
{
	[GameLib.CodeEnumClass.Esper] 			= "Icon_Windows_UI_CRB_Esper",
	[GameLib.CodeEnumClass.Medic] 			= "Icon_Windows_UI_CRB_Medic",
	[GameLib.CodeEnumClass.Stalker] 		= "Icon_Windows_UI_CRB_Stalker",
	[GameLib.CodeEnumClass.Warrior] 		= "Icon_Windows_UI_CRB_Warrior",
	[GameLib.CodeEnumClass.Engineer] 		= "Icon_Windows_UI_CRB_Engineer",
	[GameLib.CodeEnumClass.Spellslinger] 	= "Icon_Windows_UI_CRB_Spellslinger",
}

local class_tooltips = 
{
	[GameLib.CodeEnumClass.Esper] 			= "CRB_Esper",
	[GameLib.CodeEnumClass.Medic] 			= "CRB_Medic",
	[GameLib.CodeEnumClass.Stalker] 		= "CRB_Stalker",
	[GameLib.CodeEnumClass.Warrior] 		= "CRB_Warrior",
	[GameLib.CodeEnumClass.Engineer] 		= "CRB_Engineer",
	[GameLib.CodeEnumClass.Spellslinger] 	= "CRB_Spellslinger",
}

OneJob.roleUnknown = 0
OneJob.roleDPS = 1
OneJob.roleHealer = 2
OneJob.roleTank = 3

local role_icons =
{
	[0] = "",
	[1] = "sprRaid_Icon_RoleDPS",
	[2] = "sprRaid_Icon_RoleHealer",
	[3] = "sprRaid_Icon_RoleTank",
}

local role_tooltips =
{
	[0] = "",
	[1] = "Matching_Role_DPS",
	[2] = "Matching_Role_Healer",
	[3] = "Matching_Role_Tank",
}

local function count_dict(the_dict)
	if not type(the_dict) == "table" then
		return nil
	end

	local count = 0
	for _, _ in pairs(the_dict) do
		count = count + 1
	end
	return count
end

-------------------------------
-- Player definition class
local OJPlayer = {}
function OJPlayer:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self 
	
	self.name = nil

	--if not player_class then
	--	-- try to find class
	--	local party_search = find_class_by_name_in_party(player_name) 
	--	if party_search then
	--		player_class = party_search
	--	end
	--end

	self.class = nil
	self.role = nil
	
	return o
end



function OneJob:collectPartyInfo()
	local party_members = {}
	local party_size = GroupLib.GetMemberCount()
	--if party_size == 0 then -- not in a party
	--	log:warn('Empty group, not building party member list.')
	--	return nil
	--end

	if self.known_players == nil then
		self.known_players = {}
	end

	log:debug('Building party member table with '..party_size..' members.')
	for idx = 1, party_size do
		local member_data = GroupLib.GetGroupMember(idx)
		local char_name = member_data.strCharacterName
		local char_class = member_data.eClassId
		local char_role = self:findRoleByData(member_data) -- reading role isn't just one call, so extra function
		party_members[idx] = member_data

		-- Update our historical data
		if self.known_players[char_name] == nil then
			self.known_players[char_name] = OJPlayer:new()
		end
		self.known_players[char_name].name = char_name --superfluous?
		self.known_players[char_name].class = char_class --superfluous?
		if self.known_players[char_name].role ~= self.roleUnknown and char_role ~= self.roleUnknown then
			self.known_players[char_name].role = char_role --superfluous?
		end
	end
	log:debug('Membertable updated successfully.')
	self.party_members = party_members
	return self.party_members
end

function OneJob:getPartyMemberData(desired_name)
	if self.party_members == nil then
		self.party_members = {}
	end
	for idx, member_data in pairs(self.party_members) do
		if member_data.strCharacterName == desired_name then
			return member_data
		end
	end
	return nil -- player not found
end

function OneJob:findClassByName(given_name)
	if given_name == nil then
		return false
	end
	if self.known_players == nil then
		self.known_players = {}
	end
	if  self.known_players[given_name] == nil then
		log:info("Couldn't locate player "..given_name.." to determine class.")
		return nil
	end

	return self.known_players[given_name].class
end

function OneJob:findRoleByName(given_name)
	if given_name == nil then
		return false
	end
	if self.known_players == nil then
		self.known_players = {}
	end
	if self.known_players[given_name] == nil then
		log:info("Couldn't locate player "..given_name.." to determine class.")
		return nil
	end

	return self.known_players[given_name].role
end


function OneJob:findRoleByData(member_data)
	if not member_data.strCharacterName then
		log:error('Invalid table given. Cannot determine role from that.')
		return nil
	end
	-- find out his class
	local player_name = member_data.strCharacterName
	if member_data.bDPS then
		log:info('Player '..player_name..' is a damagedealer.')
		return self.roleDPS
	elseif member_data.bHealer then
		log:info('Player '..player_name..' is a healer.')
		return self.roleHealer
	elseif member_data.bTank then
		log:info('Player '..player_name..' is a tank.')
		return self.roleTank
	end
	
	--log:info("Couldn't locate player "..player_name.." to determine role.")
	return self.roleUnknown  -- no role set or player not found in party.
end


function OneJob:buildPlayerDataFromCache(char_name)
	local char_data = OJPlayer:new()
	char_data.name = char_name --superfluous?
	char_data.strCharacterName = char_name --superfluous?
	char_data.class = self:findClassByName(char_name) or -1 --superfluous?
	char_data.eClassId = char_data.class -- imitate group data
	char_data.role = self:findRoleByName(char_name) or self.roleUnknown --superfluous?
	return char_data

end


----------------------------------
-- Group definition class
local OJGroup = {}
function OJGroup:new(o, group_type, group_num, group_notes)
	o = o or {}
	setmetatable(o, self)
	self.__index = self 
	
	self.type = group_type
	self.num = group_num
	self.notes = group_notes

	self.assignments = {}
	self.known_players = {}
	
	return o
end

function OJGroup:getType() --deprecated
	log:info('Deprecated call')
	return self.type
end

function OJGroup:getNum() --deprecated
	log:info('Deprecated call')
	return self.num
end

function OneJob:getCombinedGroupName(group_object)
	if group_object == nil then
		return ""
	else
		return group_object.type..group_object.num
	end
end

--function OJGroup:getNotes() --deprecated
--	log:info('Deprecated call')
--	return self.notes
--end

function OneJob:addPlayerToGroup(group_object, player_object)
	local player_name = player_object.strCharacterName
	if player_name == nil or player_name == "" then
		return nil
	end
	log:debug('Adding player '..player_name..' to group '..group_object.type..group_object.num)
	group_object.assignments[player_name] = player_object
end

function OneJob:removePlayerFromGroup(group_object, player_object)
	local player_name = player_object
	if type(player_object) == "table" then
		player_name = player_object.name
	end

	if player_name == nil or player_name == "" then
		return nil
	end

	log:debug('Removing player '..player_name..' from group '..self:getCombinedGroupName(group_object))
	group_object.assignments[player_name] = nil
end

function OneJob:getAssignmentsFromGroup(group_object)
	local name_list = {}
	for idx, current_player in pairs(group_object.assignments) do
		if current_player.name then
			name_list[current_player.name] = current_player
		end
	end
	return name_list
end

function OneJob:getGroupSize(group_object)
	local count = 0
	for _, _ in pairs(group_object.assignments) do
		count = count + 1
	end
	return count
end


----------------------------------
-- Encounter definition class
local OJEncounter = {}
function OJEncounter:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self 
	
	self._groups_by_type = {}
	
	return o
end

function OJEncounter:setName(encounter_name) --deprecated
	self._encounter_name = encounter_name
end

function OJEncounter:getName(encounter_name) --deprecated
	return self._encounter_name
end

function OneJob:addGroupToEncounter(encounter, group_type, group_num, notes)

	if group_type == nil or group_type == "" then
		log:error("Can't add group with invalid type.")
		return nil
	end

	if not type(encounter) == "table" then
		encounter = self:findEncounterByName(encounter)
	end
	if encounter == nil then
		return nil
	end

	if encounter._groups_by_type == nil then
		encounter._groups_by_type = {}
	end

	if encounter._groups_by_type[group_type] == nil then  -- check if first of a type, if so, create the type dict
		encounter._groups_by_type[group_type] = {}
	end

	if group_num == nil then
		group_num = 1
	end

	-- try to find next free number
	local free_num_found = false
	while (not free_num_found) do
		if encounter._groups_by_type[group_type][group_num] == nil then
			free_num_found = true
		else
			group_num = group_num + 1
		end
	end

	local new_group = OJGroup:new()
	new_group.type = group_type
	new_group.num = group_num
	new_group.notes = notes or ""
	new_group.assignments = {}

	encounter._groups_by_type[group_type][group_num] = new_group -- add the group object
end

function OneJob:removeGroupFromEncounter(encounter, group_type, group_num)
	if not encounter or not group_type or not group_num then
		log:error("received invalid data, not removing any group.")
		return nil
	end
	if encounter._groups_by_type[group_type] == nil then
		log:error("Specified group type does not exist. Not removing anything.")
		return nil
	end

	for idx, current_group in pairs(encounter._groups_by_type[group_type]) do
		if tonumber(current_group.num) == tonumber(group_num) then
			encounter._groups_by_type[group_type][idx] = nil
			return true
		end
	end
	log:warn("Did not fine requested num "..group_num.." in types "..group_type)
	return nil
end

function OneJob:announceGroupTypeToParty(encounter_object, group_object)
	if group_object == nil or group_object == "" then
		return nil
	end

	if not type(group_object) == 'table' then
		-- TODO find group from name
		return nil -- until then error out
	end
	
	if not type(encounter_object) == "table" then
		encounter_object = self:findEncounterByName(encounter_object)
	end
	if encounter_object == nil then
		log:error('Could not look up encounter object.')
		return nil
	end

	if group_object.assignments == nil then
		group_object.assignments = {}
	end

	local name_list = ""
	for idx,player_data in pairs(group_object.assignments) do
		if player_data.name ~= nil then
			name_list = name_list..player_data.name..", "
		else
			log:warn("Cannot announce player object with missing name.")
		end
	end

	if name_list == "" then
		name_list = "(Noone assigned)"
	else
		name_list = string.sub(name_list, 1, -3).."." -- cut off last comma, add a finishing dot.
	end

	local output_string = group_object.type.." "..group_object.num..": "..name_list
	--log:info(output_string)
	ChatSystemLib.Command("/"..self.output_channel.." "..output_string)

end

function OJEncounter:getGroupsByType() --deprecated
	return self._groups_by_type
end

----------------------------------------
-- Main addon definition
function OneJob:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self

	self._encounters = {}

	self._panels = {}  -- contains the gridpanels

	self.selected_encounter = nil
	self.selected_group = nil
	self.selected_assignment = nil
	self.new_encounter_name = "" -- gui support var
	self.new_group_type = "" -- gui support var
	self.output_channel = "p" -- default to party output

	return o
end

function OneJob:Init()
	if self._encounters == nil then
		self._encounters = {}
	end
	self._panels = {}
	self.selected_encounter = nil
	self.selected_group = nil
	self.selected_assignment = nil
	self.new_encounter_name = "" -- gui support var
	self.new_group_type = "" -- gui support var
	self.output_channel = "p" -- default to party output
end

function OneJob:OnDependencyError(strDep, strError)
	return false
end

function OneJob:OnInitialize()

	if self._encounters == nil then
		self._encounters = {}
	end
	self._panels = {}
	self.selected_encounter = nil
	self.selected_group = nil
	self.selected_assignment = nil
	self.new_encounter_name = "" -- gui support var
	self.new_group_type = "" -- gui support var
	self.output_channel = "p" -- default to party output

	self.GeminiLogging = Apollo.GetPackage('Gemini:Logging-1.2').tPackage
	local loglevel = self.GeminiLogging.WARN

	-- Check if we should enable DEBUG mode
	local playerunit = GameLib.GetPlayerUnit()
	if playerunit ~= nil and (playerunit:GetName() == 'Rhicola' or playerunit:GetName() == 'Chala') then
		loglevel = self.GeminiLogging.DEBUG
	end

	log = self.GeminiLogging:GetLogger({
		level = loglevel,
		pattern = "%d %n %c %l - %m",
		appender = "GeminiConsole"
	})

	self.xmlDoc = XmlDoc.CreateFromFile("OneJobForms.xml")

	self.GeminiGUI = Apollo.GetPackage("Gemini:GUI-1.0").tPackage
	if not self.GeminiGUI then
		log:error('Unable to load the GeminiGUI library. This means no GUI for you and a rain of unexpected errors incoming.')
		--return true
	end

	self.dlg = Apollo.GetPackage("DialogLib-1.0").tPackage
	
	log:debug("Creating windows")
	self.wndMain = self.GeminiGUI:Create(self:CreateMainWindow()):GetInstance(self)
	self.wndMain:Show(false)
	-- find some container objects and store a link to their xml object. used right below.
	self.wndEncounterContainer = self.wndMain:FindChild('EncounterContainer') -- grid listing all encounters
	self.wndGroupContainer = self.wndMain:FindChild('GroupContainer') -- grid listing all groups for selected encounter
	self.wndAssignmentContainer = self.wndMain:FindChild('AssignmentContainer') -- grid listing all assigned people for selected group
	self.wndPartyContainer = self.wndMain:FindChild('PartyContainer') -- grid listing all available party members
	-- find some commonly used elements and store a link to their xml object
	self.gridEncounters = self.wndEncounterContainer:FindChild('EncounterGrid')
	self.gridGroups = self.wndGroupContainer:FindChild('GroupGrid')
	self.gridAssignments = self.wndAssignmentContainer:FindChild('AssignmentGrid')
	self.gridParty = self.wndPartyContainer:FindChild('PartyGrid')

	-- register slash commands
	Apollo.RegisterSlashCommand("oj", "OnSlashcommand", self)
	Apollo.RegisterSlashCommand("onejob", "OnSlashcommand", self)
	-- these custom events cause reloading parts of the ui
	Apollo.RegisterEventHandler("OJReloadEncounters", "OnLoadEncounters", self)
	Apollo.RegisterEventHandler("OJReloadGroups", "OnLoadGroups", self)
	Apollo.RegisterEventHandler("OJReloadAssignments", "OnLoadAssignments", self)
	Apollo.RegisterEventHandler("OJReloadParty", "OnPartyChange", self)
	-- these game events cause reloading group data
	self:RegisterBucketEvent(
		{
			"Group_Updated", 
			"Group_Join", 
			"Group_Left", 
			"Group_Remove", 
			"Group_MemberFlagsChanged"
		}, 5, "OnPartyChange")  --throttle party changes
end

function OneJob:OnConfigure()
	Event_FireGenericEvent("OJReloadEncounters")
	self.wndMain:Show(true)
end

function OneJob:OnSlashcommand(cmd, arg)
	if arg == nil or arg == "" then
		Event_FireGenericEvent("OJReloadEncounters")
		self.wndMain:Show(true) -- show the window
	elseif arg == "partyrefresh" or arg == "refreshparty" then
		Event_FireGenericEvent("OJReloadParty")
	elseif arg == "debug" then
		log:warn('Enabling debug mode temporarily')
		log.level = self.GeminiLogging.DEBUG
		log:debug('Done.')
	end
end

function OneJob:findEncounterByName(encounter_name)
	if encounter_name == nil or encounter_name == "" then
		return nil
	end
	for idx, current_encounter in pairs(self:getEncounters()) do
		if current_encounter._encounter_name == encounter_name then
			return current_encounter -- found a match
		end
	end
	return nil -- nothing found if we reach here.
end

function OneJob:OnLoadEncounters()
	log:debug('Caught OJEvent: OJReloadEncounters')
	if not self.wndMain then
		return nil -- Window isn't built yet
	end
	local containerEncounters = self.wndMain:FindChild('EncounterContainer')
	local gridEncounters = containerEncounters:FindChild('EncounterGrid')
	gridEncounters:DeleteAll() -- empty grid completely
	local all_encounters = self:getEncounters()
	local enc_count = 0
	for idx, current_encounter in pairs(all_encounters) do
		if current_encounter._encounter_name then
			enc_count = enc_count + 1
			-- insert a new encounter row
			local current_row = gridEncounters:AddRow('')
			gridEncounters:SetCellData(current_row, 1, current_encounter._encounter_name)
			gridEncounters:SetCellText(current_row, 1, current_encounter._encounter_name)
		else
			log:error('Invalid encounter object. Skipped.')
		end
	end
	log:debug("Populated EncounterGrid with "..enc_count.." encounters.")
end

function OneJob:OnLoadGroups()
	if self.selected_encounter == nil then
		log:info('No encounter selected. Not loading groups.')
		self.selected_group = nil
		if self.gridGroups then -- Cleanup displayed group and assignments
			self.gridGroups:DeleteAll()
			Event_FireGenericEvent("OJReloadAssignments")
		end
		return nil
	end
	self.gridGroups:DeleteAll()
	local all_groups = self:getGroupsForEncounter(self.selected_encounter)
	if all_groups == nil then
		return nil
	end
	local grp_count = 0
	for group_type, current_groups in pairs(all_groups) do
		for idx, current_group in pairs(current_groups)	do
			if current_group.type then
				grp_count = grp_count + 1
				-- insert new group row
				local current_row = self.gridGroups:AddRow('')
				self.gridGroups:SetCellData(current_row, 1, current_group.type)
				self.gridGroups:SetCellText(current_row, 1, current_group.type) -- group type
				self.gridGroups:SetCellText(current_row, 2, current_group.num) -- group num
				self.gridGroups:SetCellText(current_row, 3, count_dict(current_group.assignments)) -- TODO: Fetch the amount of assignments in group
				self.gridGroups:SetCellText(current_row, 4, current_group.notes) -- group notes
			else
				log:error('Invalid group object. Skipped.')
			end
		end
	end
	log:debug("Populated GroupGrid with "..grp_count.." groups.")
end

function OneJob:OnLoadAssignments()
	if not self.selected_encounter or not self.selected_group then
		log:info('Need both an encounter and group selected. Not loading assignments.')
		if not self.gridAssignments == nil then -- Cleanup displayed group and assignments
			self.gridAssignments:DeleteAll()
		end
		return nil
	end
	local assignments = self:getAssignmentsFromGroup(self.selected_group)
	if assignments == nil then
		log:error('Invalid assignments received. Internal error.')
		return nil
	end

	self.gridAssignments:DeleteAll()
	local assign_count = 0
	for char_name, char_data in pairs(assignments) do
		if type(char_data) == "table" then
			assign_count = assign_count + 1
			local current_row = self.gridAssignments:AddRow('')
			self.gridAssignments:SetCellData(current_row, 1, char_name)
			self.gridAssignments:SetCellText(current_row, 1, char_name) -- group type
			if char_data.role == nil then
				char_data.role = self.roleUnknown
			end
			local role_icon = role_icons[char_data.role]
			--log:debug('Role is '..char_data.role)
			if role_icon ~= nil then
				self.gridAssignments:SetCellImage(current_row, 2, role_icon) -- group num
			end
			local class_icon = class_icons[char_data.class]
			--log:debug('Class is '..char_data.class)
			if class_icon ~= nil then
				self.gridAssignments:SetCellImage(current_row, 3, class_icon) -- group num
			end
			--self.gridAssignments:SetCellText(current_row, 3, char_data.class) -- TODO: Fetch the amount of assignments in group
		else
			log:debug("char_data is type: "..type(char_data).." with content: "..char_data)
		end
	end
	log:debug("Shown "..assign_count.." assignments in grid.")
end

function OneJob:OnPartyChange()
	log:debug("OnPartyChange called")
	self:collectPartyInfo() -- update internal player tables
	if self.gridParty == nil then
		return nil
	end
	-- Recreate all party rows
	self.gridParty:DeleteAll()
	for idx, member_data in pairs(self.party_members) do
		local member_name = member_data.strCharacterName
		local char_data = self.known_players[member_name]
		local current_row = self.gridParty:AddRow('')
		self.gridParty:SetCellData(current_row, 1, member_name)
		if char_data.role == nil then
			char_data.role = self.roleUnknown
		end
		local role_icon = role_icons[char_data.role]
		log:debug('Role is '..char_data.role)
		if role_icon ~= nil then
			self.gridParty:SetCellImage(current_row, 2, role_icon) -- group num
		end
		local class_icon = class_icons[char_data.class]
		log:debug('Class is '..char_data.class)
		if class_icon ~= nil then
			self.gridParty:SetCellImage(current_row, 3, class_icon) -- group num
		end
	end
end

function OneJob:OnSave(eLevel)
	if eLevel ~= GameLib.CodeEnumAddonSaveLevel.Account then -- don't save per-character
		--log:debug("Not saving to character-specific storage.")
		return nil
	end
	local saveData = {}
	
	saveData['version'] = 1 -- store a structure version, for later data migrations
	if self._encounters == nil then
		self._encounters = {}
	end
	saveData['encounters'] = self._encounters
	
	if self.known_players == nil then
		self.known_players = {}
	end
	saveData['known_players'] = self.known_players

	if self.output_channel == nil then
		self.output_channel = "p"
	end
	saveData['output_channel'] = self.output_channel

	log:debug("Saving now..")
	return saveData
end

function OneJob:OnRestore(eLevel, tData)
	if eLevel ~= GameLib.CodeEnumAddonSaveLevel.Account then -- nothing to load per-character
		--log:debug("Only loading from account storage.")
		return nil
	end
	
	log:debug("Loading now...")
	if tData['version'] and tData['version'] == 1 then
		if tData['encounters'] and type(tData['encounters']) == 'table' then
			self._encounters = tData['encounters']
		end
		if tData['known_players'] and type(tData['known_players']) == 'table' then
			self.known_players = tData['known_players']
		end
		if tData['output_channel'] and type(tData['output_channel']) == 'table' then
			self.output_channel = tData['output_channel']
		end
		--Event_FireGenericEvent("OJReloadEncounters", tData)
	end
	log:debug("...done.")
end

function OneJob:addEncounter(encounter_name)
	-- check if invalid name
	if encounter_name == nil or encounter_name == "" then
		log:error('Will not add encounter with empty name.')
		return nil
	end
	--check if duplicate name
	if not self:findEncounterByName(encounter_name) == nil then
		log:warn('An encounter with the name "'..encounter_name..'" exists already! Ignoring request.')
		return nil
	end
	local new_encounter = OJEncounter:new()
	new_encounter._encounter_name = encounter_name
	self._encounters[encounter_name] = new_encounter
	log:debug("Added encounter "..encounter_name)
end

function OneJob:deleteEncounter(encounter_name)
	if not self._encounters[encounter_name] then
		log:debug("Unable to remove encounter "..encounter_name.." as it does'nt exist.")
		return false
	end
	self._encounters[encounter_name] = nil
	log:debug("Removed encounter "..encounter_name)
	Event_FireGenericEvent("OJReloadEncounters")
	return true
end

function OneJob:getEncounters()
	if not self._encounters then
		return {}
	else 
		return self._encounters
	end
end

function OneJob:getGroupsForEncounter(encounter_name)
	if encounter_name == nil or encounter_name == "" then
		if self.selected_encounter == nil then
			log:warn('Unable to find groups for undefined encounter.')
			return nil
		end
		encounter_name = self.selected_encounter._encounter_name
	end

	local current_encounter = encounter_name
	if not type(current_encounter) == "table" then
	 	current_encounter = self:findEncounterByName(encounter_name)
	end

	if current_encounter == nil then
		return {}
	else
		return current_encounter._groups_by_type
	end
end

function OneJob:getGroup(encounter_object, group_type, group_num)
	-- fix encounter_object if we getthe name only
	if not type(encounter_object) == "table" then
		encounter_object = self:getEncounters()[encounter_object]
	end
	local encounter_name = encounter_object._encounter_name
	if encounter_object._groups_by_type[group_type] == nil then
		log:error('No group type "'..group_type..'" found in encounter '..encounter_name)
		return nil
	end
	local groups_to_search_thru = encounter_object._groups_by_type[group_type]
	for _, current_group in pairs(groups_to_search_thru) do
		if current_group and current_group.num then
			if tonumber(current_group.num) == tonumber(group_num) then
				return current_group
			end
		end
	end
	log:error('No group num "'..group_num..'" found in encounter '..encounter_name)
	return nil -- didn't find group
end

-----

local function onEncounterGridSelChange(oAddon, wndHandler, wndControl, iRow, iCol)
	local chosen_encounter_name = wndControl:GetCellText(iRow, iCol)
	if not chosen_encounter_name then
		log:error('No Celltext found for row '..iRow..' and col '..iCol)
		return false
	end
	log:debug('Encounter grid selection change to row '..iRow..' and col '..iCol..' encounter: '..chosen_encounter_name)
	oAddon.selected_encounter = oAddon:findEncounterByName(chosen_encounter_name)
	--oAddon:doGroupUpdate(chosen_encounter)
	Event_FireGenericEvent("OJReloadGroups")
end

-----
function OneJob:doGroupGridUpdate(encounter_object)
	-- TODO: Trigger an update on self.wndGroupContainer, make it list all of the groups.
	-- TODO: Optionally, clear assignment panel
	log:debug("doGroupUpdate called.")
	if not self.wndMain then
		log:warn('Unable to find window. Cancelling GroupGridUpdate.')
		return nil
	end
	local group_control = self.gridGroups
	if not group_control then
		log:error('Unable to locate group grid.')
		return nil
	end

	if encounter_object == nil then
		log:debug("Emptying group grid...")
		-- Fill Grid with a notice that it's empty.
		group_control:SetCellText(1, 1, 'No encounter selected.')
		group_control:SetCellText(1, 2, '')
		group_control:SetCellText(1, 3, '')
		group_control:SetCellText(1, 4, '')
		-- TODO: Clear all existing rows... how do I get a count?
		log:debug('...done.')
		return true
	end

	log:debug('Populating group grid now...')
	local all_groups = encounter_object:getGroupsByType()
	for idx, current_group in all_groups do
		local current_row = group_control:AddRow('')
		group_control:SetCellData(current_row, 1, current_group)
		group_control:SetCellText(current_row, 1, current_group.type) -- interrupters
		group_control:SetCellText(current_row, 2, current_group.num or "-1") -- 2
		--group_control:SetCellText(current_row, 3, self:getAssignmentsCount(current_group)) -- 5 -- TODO: Implement this
		group_control:SetCellText(current_row, 4, "wtfbbq") -- "healers+tanks"
	end
	log:debug('...done. ('..#all_groups..' groups)')
end

local function populateGroupGrid(oAddon, wndHandler, wndControl)
	oAddon:doGroupGridUpdate(nil) -- 
end

local function onGroupGridSelChange(oAddon, wndHandler, wndControl, iRow, iCol, eClick)
	
	local chosen_group_type = wndControl:GetCellText(iRow, 1)
	if chosen_group_type == nil or chosen_group_type == "" then
		log:error('No Celldata found for row '..iRow..' and col 1')
		return false
	end
	local chosen_group_num = wndControl:GetCellText(iRow, 2)
	log:debug('Group grid selection change to row '..iRow..' and col '..iCol)
	log:debug("type: "..chosen_group_type.." num: "..chosen_group_num)
	--if oAddon.selected_encounter._groups_by_type[chosen_group_type] == nil then
	--	log:error("No such group type found in encounter "..oAddon.selected_encounter.name)
	--	return nil
	--end
	--oAddon.debug_var = oAddon.selected_encounter._groups_by_type[chosen_group_type]
	--if oAddon.selected_encounter._groups_by_type[chosen_group_type][chosen_group_num] == nil then
	--	log:error("No such group number found in encounter "..oAddon.selected_encounter._encounter_name)
	--	return nil
	--end
	oAddon.selected_group = oAddon:getGroup(oAddon.selected_encounter, chosen_group_type, chosen_group_num)
	--oAddon.selected_group = oAddon.selected_encounter._groups_by_type[chosen_group_type][chosen_group_num]
	Event_FireGenericEvent("OJReloadAssignments")

	if eClick == 1 then
		--additional rightlcick actions
	    -- make sure only one context window is active at a time
	    if oAddon.wndContext ~= nil and oAddon.wndContext:IsValid() then
	        oAddon.wndContext:Destroy()
	    end

	    -- Create a context menu and bring it to front
	    --oAddon.wndContext = Apollo.LoadForm(oAddon.xmlDoc, "EPGPContextMenu", oAddon.wndMain, oAddon)
	    oAddon.wndContext = oAddon.GeminiGUI:Create(oAddon:CreateGroupContextMenu()):GetInstance(oAddon, oAddon.wndGroupContainer)
	    oAddon.wndContext:ToFront()


	    -- Move context menu to the mouse
	    local tCursor= wndHandler:GetMouse()
	    local windowX, windowY = oAddon.wndMain:GetPos()
	    local containerX, containerY = oAddon.wndGroupContainer:GetPos()
	    local handlerX, handlerY = wndHandler:GetPos()
	    --local moveToX = tCursor.x + handlerX + windowX + containerX
	    --local moveToY = tCursor.y + handlerY + windowY + containerY
	    local moveToX = tCursor.x
	    local moveToY = tCursor.y
	    log:debug("Moving context window to X:"..moveToX.." Y:"..moveToY)
		oAddon.wndContext:Move(moveToX, moveToY , oAddon.wndContext:GetWidth(), oAddon.wndContext:GetHeight())
		oAddon.wndContext:ToFront()

	    -- Save the name so we can use it for whatever action we choose (Could save iRow and look up whatever instead)
	    oAddon.wndContext:SetData(wndHandler:GetCellText(iRow, 1))
	end

end


local function onGroupGridItemClick(oAddon, wndControl, wndHandler, iRow, iCol, eClick)

end

function OneJob:GroupNotesEditCallback(iButtonId, strInput)
	--log:debug("strButtonId is: "..iButtonId)
	if iButtonId == 2 then -- cancel
		log:info("Cancelled notes edit.")
		return
	end

	local oAddon = self.dialogAddon

	if oAddon.group_to_set_notes == nil then -- self is actually dialoglib... bug?
		log:error("No group selected. Won't set note!")
		return false
	end
	--log:info("Would set note to "..strInput)
	local selected_group = oAddon.group_to_set_notes
	if selected_group.notes ~= nil then
		selected_group.notes = strInput
		Event_FireGenericEvent("OJReloadGroups")
	end
end

function OneJob:OnGroupNotesEditButtonClick(wndControl, wndHandler)
	-- close the context window first
	wndControl:GetParent():Close() 
	if self.selected_group == nil then
		log:warn("No group selected. Not editing notes.")
		return false
	end

	self.group_to_set_notes = self.selected_group -- saving out reference incase user swaps group selection while dialog is open

	self.dlg:ShowDialog(
		"TextInput", -- template
		"Enter new notes", --dialogtext
		{
			green= "OK", 
			red= "Cancel"
		},
		"GroupNotesEditCallback",
		self
	)
end

local function onAssignmentGridSelChange(oAddon, wndHandler, wndControl, iRow, iCol)
	local chosen_assignment = wndControl:GetCellText(iRow, 1)
	if chosen_assignment == nil or chosen_assignment == "" then
		log:error('No Celldata found for row '..iRow..' and col 1')
		return false
	end
	log:debug('Group grid selection change to row '..iRow..' and col '..iCol)
	oAddon.selected_assignment = chosen_assignment
end


local function onPartyGridSelChange(oAddon, wndHandler, wndControl, iRow, iCol)
	local chosen_name = wndControl:GetCellText(iRow, 1)
	if chosen_name == nil or chosen_name == "" then
		log:error('No Celldata found for row '..iRow..' and col 1')
		return false
	end
	log:debug('Party grid selection change to row '..iRow..' and col '..iCol)
	oAddon.player_to_add = chosen_name
end

function OneJob:addEncounterButtonClicked()
	if self.new_encounter_name then
		log:debug("Adding encounter named: "..self.new_encounter_name)
		self:addEncounter(self.new_encounter_name)
		log:debug("...done")
		Event_FireGenericEvent("OJReloadEncounters")
	end
end

function OneJob:removeEncounterButtonClicked(wndHandler, wndControl)
	if self.selected_encounter == nil then
		log:info('No encounter selected. Removing nothing.')
		return nil
	end
	self:deleteEncounter(self.selected_encounter._encounter_name)
	self.selected_encounter = nil
	Event_FireGenericEvent("OJReloadEncounters")
	Event_FireGenericEvent("OJReloadGroups")
end

function OneJob:addGroupButtonClicked()
	if self.new_group_type then
		log:debug("Adding group type: "..self.new_group_type)
		self:addGroupToEncounter(self.selected_encounter, self.new_group_type, 1, "") -- TODO: allow to enter notes
		log:debug("...done")
		Event_FireGenericEvent("OJReloadGroups")
	end
end

function OneJob:removeGroupButtonClicked(wndHandler, wndControl)
	if self.selected_group == nil then
		log:info('No group selected. Removing nothing.')
		return nil
	end
	self:removeGroupFromEncounter(self.selected_encounter, self.selected_group.type, self.selected_group.num)
	self.selected_group = nil
	Event_FireGenericEvent("OJReloadGroups")
end

function OneJob:addPlayerToGroupButtonClicked(wndHandler, wndControl)
	if self.player_to_add == nil or self.player_to_add == "" then
		log:error("No player selectd. Will not add.")
		return nil
	end
	local player_data = self:getPartyMemberData(self.player_to_add)
	if player_data == nil or not type(player_data) == "table" then
		--log:info("Add of ungrouped players not yet supported. Coming soon.")
		--return nil
		player_data = self:buildPlayerDataFromCache(self.player_to_add)
	else
		player_data = self:buildPlayerDataFromCache(player_data.strCharacterName)
	end
	self:addPlayerToGroup(self.selected_group, player_data)
	Event_FireGenericEvent("OJReloadGroups")
	Event_FireGenericEvent("OJReloadAssignments")
end

function OneJob:removeAssignmentButtonClicked(wndHandler, wndControl)
	if self.selected_group == nil or self.selected_group == "" then
		log:info('No group selected. Removing nothing.')
		return nil
	end
	if self.selected_assignment == nil or self.selected_assignment == "" then
		log:info('No assignment selected. Removing nothing.')
		return nil
	end
	self:removePlayerFromGroup(self.selected_group, self.selected_assignment)
	self.selected_assignment = nil
	Event_FireGenericEvent("OJReloadAssignments")
end

-----------------------------------------------------
-- Export from Houston for GeminiGUI
function OneJob:CreateMainWindow()
	local tMainFormDef = {
	AnchorOffsets = { 60, 61, 1076, 645 },
	RelativeToClient = true, 
	BGColor = "UI_WindowBGDefault", 
	TextColor = "UI_WindowTextDefault", 
	Template = "CRB_NormalFramedThick_StandardHdrFtr", 
	Name = "MainForm", 
	Border = true, 
	Picture = true, 
	SwallowMouseClicks = true, 
	Moveable = true, 
	Escapable = true, 
	Overlapped = true,
	--NewWindowDepth = 1, 
	Pixies = {
		{ 
			Text          = self.addon_name.." (v"..self.addon_version..")",  -- TITLE
			--Text          = self.addon_name,  -- TITLE
			Font          = "CRB_HeaderHuge",
			TextColor     = "xkcdAcidGreen",
			AnchorPoints  = "HFILL",
			DT_CENTER     = true,
			DT_VCENTER    = true,
			AnchorOffsets = {0,0,0,40}, 
		},
	},
	Children = {
		{
			AnchorOffsets = { -25, 0, 3, 25 },
			AnchorPoints = { 1, 0, 1, 0 },
			Class = "Button", 
			Base = "CRB_UIKitSprites:btn_close", 
			Font = "DefaultButton", 
			ButtonType = "PushButton", 
			DT_VCENTER = true, 
			DT_CENTER = true, 
			BGColor = "UI_BtnBGDefault", 
			TextColor = "UI_BtnTextDefault", 
			NormalTextColor = "UI_BtnTextDefault", 
			PressedTextColor = "UI_BtnTextDefault", 
			FlybyTextColor = "UI_BtnTextDefault", 
			PressedFlybyTextColor = "UI_BtnTextDefault", 
			DisabledTextColor = "UI_BtnTextDefault", 
			Name = "CloseButton", 
			NoClip = true, 
			Events = {
				ButtonSignal = function(_, wndHandler, wndControl) -- anonymous function for an event handler
					wndControl:GetParent():Close() 
				end, 
			},
		},
		{
			AnchorOffsets = { 10, 47, 160, -80 },
			AnchorPoints = { 0, 0, 0, 1 },
			RelativeToClient = true, 
			BGColor = "UI_WindowBGDefault", 
			TextColor = "UI_WindowTextDefault", 
			Template = "HologramControl1", 
			Name = "EncounterContainer", 
			IgnoreMouse = true, 
			UseTemplateBG = true, 
			Border = true, 
			Children = {
				{
					Name = "EncounterGrid",
					WidgetType = 'Grid',
					RowHeight = 32,
					AnchorOffsets = { 1, 1, 1, -90 },
					AnchorPoints = { 0, 0, 1, 1 },
					Class = "Grid", 
					RelativeToClient = true, 
					--BGColor = "UI_WindowBGDefault", 
					TextColor = "UI_WindowTextDefault", 
					Template = "HologramControl1",  
					HeaderRow = true, 
					SelectWholeRow = true, 
					Columns = {
						{ Name = "Encounter", Width = 134, TextColor = "White", MinWidth = 10, MaxWidth = 2048, SimpleSort = true, DT_VCENTER = true, },
					},
					Events = {
						GridSelChange = onEncounterGridSelChange,
					}
				},
				{
					AnchorOffsets = { 5, -30, -5, -5 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "Button", 
					Base = "CRB_Basekit:kitBtn_Holo", 
					Font = "DefaultButton", 
					ButtonType = "PushButton", 
					DT_VCENTER = true, 
					DT_CENTER = true, 
					BGColor = "UI_BtnBGDefault", 
					TextColor = "UI_BtnTextDefault", 
					NormalTextColor = "UI_BtnTextDefault", 
					PressedTextColor = "UI_BtnTextDefault", 
					FlybyTextColor = "UI_BtnTextDefault", 
					PressedFlybyTextColor = "UI_BtnTextDefault", 
					DisabledTextColor = "UI_BtnTextDefault", 
					Name = "RemoveEncounterButton", 
					Template = "HologramControl2", 
					TextId = "CRB_Remove", 
					Events = {
						ButtonSignal = function(oAddon, wndHandler, wndControl)
							oAddon:removeEncounterButtonClicked(wndHandler, wndControl)
						end,
					},
				},
				{
					AnchorOffsets = { 5, -55, -5, -30 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "Button", 
					Base = "CRB_Basekit:kitBtn_Holo", 
					Font = "DefaultButton", 
					ButtonType = "PushButton", 
					DT_VCENTER = true, 
					DT_CENTER = true, 
					BGColor = "UI_BtnBGDefault", 
					TextColor = "UI_BtnTextDefault", 
					NormalTextColor = "UI_BtnTextDefault", 
					PressedTextColor = "UI_BtnTextDefault", 
					FlybyTextColor = "UI_BtnTextDefault", 
					PressedFlybyTextColor = "UI_BtnTextDefault", 
					DisabledTextColor = "UI_BtnTextDefault", 
					Name = "AddEncounterButton", 
					Template = "HologramControl1", 
					TextId = "ArenaRoster_Add", 
					Events = {
						ButtonSignal = function(oAddon, wndHandler, wndControl)
							oAddon:addEncounterButtonClicked()
							wndControl:GetParent():FindChild('AddEncounterEditBox'):SetText('')
						end,
					},
				},
				{
					AnchorOffsets = { 5, -90, -5, -55 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "EditBox", 
					RelativeToClient = true, 
					Text = "New Encounter...", 
					BGColor = "UI_WindowBGDefault", 
					TextColor = "UI_WindowTextDefault", 
					Template = "HologramControl2", 
					Name = "AddEncounterEditBox", 
					Border = true, 
					Events = {
						EditBoxChanged = function(oAddon, wndHandler, wndControl, strText)
								oAddon.new_encounter_name = strText
						end,
					}
				},
			},
		},
		{
			AnchorOffsets = { 170, 47, 510, -80 },
			AnchorPoints = { 0, 0, 0, 1 },
			RelativeToClient = true, 
			BGColor = "UI_WindowBGDefault", 
			TextColor = "UI_WindowTextDefault", 
			Template = "HologramControl1", 
			Name = "GroupContainer", 
			IgnoreMouse = true, 
			UseTemplateBG = true, 
			Border = true, 
			Children = {
				{
					WidgetType = "Grid",
					AnchorOffsets = { 1, 1, 1, -120 },
					AnchorPoints = { 0, 0, 1, 1 },
					Class = "Grid", 
					RelativeToClient = true, 
					BGColor = "UI_WindowBGDefault", 
					TextColor = "UI_WindowTextDefault", 
					Template = "HologramControl1", 
					Name = "GroupGrid", 
					MultiColumn = true, 
					HeaderRow = true, 
					SelectWholeRow = true, 
					Columns = {
						{ Name = "Type", Width = 100, TextColor = "White", MinWidth = 10, MaxWidth = 2048, SimpleSort = true, DT_VCENTER = true, },
						{ Name = "Num", Width = 30, TextColor = "White", MinWidth = 10, MaxWidth = 60, SimpleSort = true, DT_VCENTER = true, },
						{ Name = "Ppl", Width = 30, TextColor = "White", MinWidth = 10, MaxWidth = 60, SimpleSort = true, DT_VCENTER = true, },
						{ Name = "Notes", Width = 128, TextColor = "White", MinWidth = 10, MaxWidth = 2048, SimpleSort = true, DT_VCENTER = true, },
					},
					Events = {
						GridSelChange = onGroupGridSelChange,
					}
				},
				{
					AnchorOffsets = { 5, -30, -5, -5 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "Button", 
					Base = "CRB_Basekit:kitBtn_Holo", 
					Font = "DefaultButton", 
					ButtonType = "PushButton", 
					DT_VCENTER = true, 
					DT_CENTER = true, 
					BGColor = "UI_BtnBGDefault", 
					TextColor = "UI_BtnTextDefault", 
					NormalTextColor = "UI_BtnTextDefault", 
					PressedTextColor = "UI_BtnTextDefault", 
					FlybyTextColor = "UI_BtnTextDefault", 
					PressedFlybyTextColor = "UI_BtnTextDefault", 
					DisabledTextColor = "UI_BtnTextDefault", 
					Name = "RemoveGroupButton", 
					Template = "HologramControl2", 
					Text = "Remove group", 
					Events = {
						ButtonSignal = function(oAddon, wndHandler, wndControl)
							oAddon:removeGroupButtonClicked(wndHandler, wndControl)
						end,
					},
				},
				{
					AnchorOffsets = { 5, -55, -5, -30 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "Button", 
					Base = "CRB_Basekit:kitBtn_Holo", 
					Font = "DefaultButton", 
					ButtonType = "PushButton", 
					DT_VCENTER = true, 
					DT_CENTER = true, 
					BGColor = "UI_BtnBGDefault", 
					TextColor = "UI_BtnTextDefault", 
					NormalTextColor = "UI_BtnTextDefault", 
					PressedTextColor = "UI_BtnTextDefault", 
					FlybyTextColor = "UI_BtnTextDefault", 
					PressedFlybyTextColor = "UI_BtnTextDefault", 
					DisabledTextColor = "UI_BtnTextDefault", 
					Name = "AddGroupButton", 
					Template = "HologramControl1", 
					Text = "Add group", 
					Events = {
						ButtonSignal = function(oAddon, wndHandler, wndControl)
							oAddon:addGroupButtonClicked()
							--wndControl:GetParent():FindChild('AddGroupEditBox'):SetText('')
						end,
					},
				},
				{
					AnchorOffsets = { 5, -90, -5, -55 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "EditBox", 
					RelativeToClient = true, 
					Text = "Enter group type to add..", 
					BGColor = "UI_WindowBGDefault", 
					TextColor = "UI_WindowTextDefault", 
					Template = "HologramControl2", 
					Name = "AddGroupEditBox", 
					Border = true, 
					Events = {
						EditBoxChanged = function(oAddon, wndHandler, wndControl, strText)
								oAddon.new_group_type = strText
						end,
					}
				},
				{
					AnchorOffsets = { 5, -120, -5, -95 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "Button", 
					Base = "CRB_Basekit:kitBtn_Holo", 
					Font = "DefaultButton", 
					ButtonType = "PushButton", 
					DT_VCENTER = true, 
					DT_CENTER = true, 
					BGColor = "UI_BtnBGDefault", 
					TextColor = "UI_BtnTextDefault", 
					NormalTextColor = "UI_BtnTextDefault", 
					PressedTextColor = "UI_BtnTextDefault", 
					FlybyTextColor = "UI_BtnTextDefault", 
					PressedFlybyTextColor = "UI_BtnTextDefault", 
					DisabledTextColor = "UI_BtnTextDefault", 
					Name = "AnnounceGroupButton", 
					Template = "HologramControl1", 
					Text = "Announce to party", 
					Events = {
						ButtonSignal = function(oAddon, wndHandler, wndControl)
							oAddon:announceGroupTypeToParty(oAddon.selected_encounter, oAddon.selected_group)
						end,
					},
				},
			},
		},
		{
			AnchorOffsets = { 520, 47, 780, -80 },
			AnchorPoints = { 0, 0, 0, 1 },
			RelativeToClient = true, 
			BGColor = "UI_WindowBGDefault", 
			TextColor = "UI_WindowTextDefault", 
			Template = "HologramControl1", 
			Name = "AssignmentContainer", 
			IgnoreMouse = true, 
			UseTemplateBG = true, 
			Border = true, 
			Children = {
				{
					WidgetType = "Grid",
					AnchorOffsets = { 1, 1, 1, -90 },
					AnchorPoints = { 0, 0, 1, 1 },
					Class = "Grid", 
					RelativeToClient = true, 
					BGColor = "UI_WindowBGDefault", 
					TextColor = "UI_WindowTextDefault", 
					Template = "HologramControl1", 
					Name = "AssignmentGrid", 
					MultiColumn = true, 
					HeaderRow = true, 
					SelectWholeRow = true, 
					Columns = {
						{ Name = "Player", Width = 100, TextColor = "White", Image = "CRB_Raid:sprRaid_Icon_RoleTank", MinWidth = 10, MaxWidth = 2048, SimpleSort = true, DT_VCENTER = true,  Text="Player", },
						{ Name = "Role", Width = 40, TextColor = "White", MinWidth = 25, MaxWidth = 40, SimpleSort = true, DT_VCENTER = true, Text="Role", },
						{ Name = "Class", Width = 40, TextColor = "White", MinWidth = 25, MaxWidth = 40, SimpleSort = true, DT_VCENTER = true, Text="Class",},
					},
					Events = {
						GridSelChange = onAssignmentGridSelChange,
					}
				},
				{
					AnchorOffsets = { 5, -30, -5, -5 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "Button", 
					Base = "CRB_Basekit:kitBtn_Holo", 
					Font = "DefaultButton", 
					ButtonType = "PushButton", 
					DT_VCENTER = true, 
					DT_CENTER = true, 
					BGColor = "UI_BtnBGDefault", 
					TextColor = "UI_BtnTextDefault", 
					NormalTextColor = "UI_BtnTextDefault", 
					PressedTextColor = "UI_BtnTextDefault", 
					FlybyTextColor = "UI_BtnTextDefault", 
					PressedFlybyTextColor = "UI_BtnTextDefault", 
					DisabledTextColor = "UI_BtnTextDefault", 
					Name = "RemoveAssignmentButton", 
					Template = "HologramControl2", 
					Text = "Remove assignment for player", 
					Events = {
						ButtonSignal = function(oAddon, wndHandler, wndControl)
							oAddon:removeAssignmentButtonClicked(wndHandler, wndControl)
						end,
					},
				},
				{
					AnchorOffsets = { 5, -55, -5, -30 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "Button", 
					Base = "CRB_Basekit:kitBtn_Holo", 
					Font = "DefaultButton", 
					ButtonType = "PushButton", 
					DT_VCENTER = true, 
					DT_CENTER = true, 
					BGColor = "UI_BtnBGDefault", 
					TextColor = "UI_BtnTextDefault", 
					NormalTextColor = "UI_BtnTextDefault", 
					PressedTextColor = "UI_BtnTextDefault", 
					FlybyTextColor = "UI_BtnTextDefault", 
					PressedFlybyTextColor = "UI_BtnTextDefault", 
					DisabledTextColor = "UI_BtnTextDefault", 
					Name = "AddManualAssignmentButton", 
					Template = "HologramControl1", 
					Text = "Add (manual) assignment", 
					Events = {
						ButtonSignal = function(oAddon, wndHandler, wndControl)
							oAddon:addPlayerToGroupButtonClicked()
							--wndControl:GetParent():FindChild('AddGroupEditBox'):SetText('')
						end,
					},
				},
				{
					AnchorOffsets = { 5, -90, -5, -55 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "EditBox", 
					RelativeToClient = true, 
					Text = "Enter player to assign...", 
					BGColor = "UI_WindowBGDefault", 
					TextColor = "UI_WindowTextDefault", 
					Template = "HologramControl2", 
					Name = "AddManualAssignmentEditBox", 
					Border = true, 
					Events = {
						EditBoxChanged = function(oAddon, wndHandler, wndControl, strText)
							oAddon.player_to_add = strText
						end,
					},
				},
			},
		},
		{
			AnchorOffsets = { 785, 47, 985, -80 },
			AnchorPoints = { 0, 0, 0, 1 },
			RelativeToClient = true, 
			BGColor = "UI_WindowBGDefault", 
			TextColor = "UI_WindowTextDefault", 
			Template = "HologramControl1", 
			Name = "PartyContainer", 
			IgnoreMouse = true, 
			UseTemplateBG = true, 
			Border = true, 
			Children = {
				{
					WidgetType = "Grid",
					AnchorOffsets = { 1, 1, 1, -35 },
					AnchorPoints = { 0, 0, 1, 1 },
					Class = "Grid", 
					RelativeToClient = true, 
					BGColor = "UI_WindowBGDefault", 
					TextColor = "UI_WindowTextDefault", 
					Template = "HologramControl1", 
					Name = "PartyGrid", 
					MultiColumn = true, 
					HeaderRow = true, 
					SelectWholeRow = true, 
					Columns = {
						{ Name = "Player", Width = 100, TextColor = "White", Image = "CRB_Raid:sprRaid_Icon_RoleTank", MinWidth = 10, MaxWidth = 2048, SimpleSort = true, DT_VCENTER = true, },
						{ Name = "Role", Width = 25, TextColor = "White", Text = "R", MinWidth = 25, MaxWidth = 25, SimpleSort = true, DT_VCENTER = true, },
						{ Name = "Class", Width = 25, TextColor = "White", Text = "C", MinWidth = 25, MaxWidth = 25, SimpleSort = true, DT_VCENTER = true, },
					},
					Events = {
						GridSelChange = onPartyGridSelChange,
					}
				},
				{
					AnchorOffsets = { 5, -30, -5, -5 },
					AnchorPoints = { 0, 1, 1, 1 },
					Class = "Button", 
					Base = "CRB_Basekit:kitBtn_Holo", 
					Font = "DefaultButton", 
					ButtonType = "PushButton", 
					DT_VCENTER = true, 
					DT_CENTER = true, 
					BGColor = "UI_BtnBGDefault", 
					TextColor = "UI_BtnTextDefault", 
					NormalTextColor = "UI_BtnTextDefault", 
					PressedTextColor = "UI_BtnTextDefault", 
					FlybyTextColor = "UI_BtnTextDefault", 
					PressedFlybyTextColor = "UI_BtnTextDefault", 
					DisabledTextColor = "UI_BtnTextDefault", 
					Name = "AddManualAssignmentButton", 
					Template = "HologramControl1", 
					Text = "Add to selected group", 
					Events = {
						ButtonSignal = function(oAddon, wndHandler, wndControl)
							oAddon:addPlayerToGroupButtonClicked()

						end,
					},
				},
			},
		},
	},
}

	return tMainFormDef
end

function OneJob:CreateGroupContextMenu()
	local tGroupContext = {
	AnchorOffsets = { 0, 0, 85, 116 },
	RelativeToClient = true, 
	Name = "EPGPContextMenu", 
	Picture = true, 
	SwallowMouseClicks = true, 
	Escapable = true, 
	Overlapped = true, 
	Sprite = "CRB_Basekit:kitBase_HoloBlue_InsetBorder_Thin", 
	CloseOnExternalClick = true, 
	Sizable = true, 
	NoClip = true, 
	NewWindowDepth = true, 
	Children = {
		{
			AnchorOffsets = { 0, 2, 0, 0 },
			AnchorPoints = { 0, 0, 1, 0.25 },
			Class = "Button", 
			Base = "CRB_Basekit:kitBtn_List_RightArrowHighlight", 
			Font = "DefaultButton", 
			ButtonType = "PushButton", 
			DT_VCENTER = true, 
			DT_CENTER = true, 
			Name = "GroupNotesEditButton", 
			Events = {
				ButtonSignal = "OnGroupNotesEditButtonClick",
			},
			Children = {
				{
					AnchorOffsets = { 20, 0, 0, 0 },
					AnchorPoints = { 0, 0, 1, 1 },
					RelativeToClient = true, 
					Font = "CRB_Interface9_O", 
					Text = "Edit Notes", 
					Name = "GroupNotesEditText", 
					TextColor = "UI_BtnTextGrayListNormal", 
					DT_VCENTER = true, 
				},
				{
					AnchorOffsets = { 4, 7, 19, -7 },
					AnchorPoints = { 0, 0, 0, 1 },
					RelativeToClient = true, 
					Name = "GroupNotesEditIcon", 
					Sprite = "CRB_BreakoutSprites:spr_BreakoutStun_ClockAnim", 
					Picture = true, 
					IgnoreMouse = true, 
				},
			},
		},
	},
}
	return tGroupContext
end

--OneJobInst = OneJob:new()
--OneJobInst:Init()
